//
//  HealthKitManager.swift
//  SampleApp
//
//  Created by Xcode User on 2018-06-12.
//  Copyright © 2018 SM. All rights reserved.
//

import Foundation
import HealthKit


class HealthKitManager{
    
    let healthStore = HKHealthStore()
    
    func authorizeHealthKit(completion: ((_ success: Bool, _ error: NSError?) -> Void)!) {
        
        // State the health data type(s) we want to read from HealthKit.
        let healthDataToRead : Set = [
            HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)!,
            HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)!,
            HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!,
            HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)!
        ]
        
        // State the health data type(s) we want to write from HealthKit.
        let healthDataToWrite : Set = [
            //HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!,
            HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)!]
        
        // Just in case OneHourWalker makes its way to an iPad...
        if !HKHealthStore.isHealthDataAvailable() {
            print("Can't access HealthKit.")
        }
        
        healthStore.requestAuthorization(toShare: healthDataToWrite, read: healthDataToRead) { (success, error) -> Void in
            if( completion != nil ) {
                completion(success, error as NSError?)
                print("Works")
            }
            else{
                print("not working ")
            }
        }
    }

    func getHeight(sampleType: HKSampleType , completion: ((HKSample?, NSError?) -> Void)!) {
        
        // Predicate for the height query
        let distantPastHeight = NSDate.distantPast as NSDate
        
        let currentDate = NSDate()
        let lastHeightPredicate = HKQuery.predicateForSamples(withStart: distantPastHeight as Date as Date, end: currentDate as Date, options: [])
        
        
        // Get the single most recent height
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        
        // Query HealthKit for the last Height entry.
        let heightQuery = HKSampleQuery(sampleType: sampleType, predicate: lastHeightPredicate, limit: 1, sortDescriptors: [sortDescriptor]) { (sampleQuery, results, error ) -> Void in
            
            if let queryError = error {
                completion(nil, queryError as NSError)
                return
            }
            
            // Set the first HKQuantitySample in results as the most recent height.
            let lastHeight = results!.first
            
            if completion != nil {
                completion(lastHeight, nil)
            }
        }
        // Time to execute the query.
        healthStore.execute(heightQuery)
    }

    func getWeight(sampleType: HKSampleType , completion: ((HKSample?, NSError?) -> Void)!){
        // Query HealthKit for the last Weight entry.
        
        let distantPastWeight = NSDate.distantPast as NSDate
        let currentDate = NSDate()
        let lastWeightPredicate = HKQuery.predicateForSamples(withStart: distantPastWeight as Date as Date, end: currentDate as Date, options: [])
        
        // Get the single most recent weight
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        
        let weightQuery = HKSampleQuery(sampleType: sampleType, predicate: lastWeightPredicate, limit: 1, sortDescriptors: [sortDescriptor]) { (sampleQuery, results, error ) -> Void in
            
            if let queryError = error {
                completion(nil, queryError as NSError)
                return
            }
            
            // Set the first HKQuantitySample in results as the most recent height.
            let lastWeight = results!.first
            
            if completion != nil {
                completion(lastWeight, nil)
            }
        }
        
        healthStore.execute(weightQuery)
    }

}

