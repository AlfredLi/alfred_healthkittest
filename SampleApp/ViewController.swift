//
//  ViewController.swift
//  SampleApp
//
//  Created by Xcode User on 2018-06-12.
//  Copyright © 2018 SM. All rights reserved.
//

import UIKit
import CoreLocation
import Foundation
import HealthKit



class ViewController: UIViewController {

    
    @IBOutlet weak var lblData: UILabel!
    @IBOutlet weak var lblWeightData: UILabel!
    @IBOutlet weak var lblHeartRateData: UILabel!
    @IBOutlet weak var lblStepCountData: UILabel!
    @IBOutlet weak var btnAuth: UIButton!
    @IBOutlet weak var btnGetData: UIButton!
    @IBOutlet weak var btnGetHeartRateData: UIButton!
    @IBOutlet weak var btnGetStepCountData: UIButton!
    
    
    
    var zeroTime = TimeInterval()
    var timer : Timer = Timer()
    
    let locationManager = CLLocationManager()
    
    var startLocation: CLLocation!
    var lastLocation: CLLocation!
    var distanceTraveled = 0.0
    
    let healthManager:HealthKitManager = HealthKitManager()
    
    var height: HKQuantitySample?
    var weight : HKQuantitySample?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setHeight() {
        // Create the HKSample for Height.
        let heightSample = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)
        
        // Call HealthKitManager's getSample() method to get the user's height.
        self.healthManager.getHeight(sampleType: heightSample!, completion: { (userHeight, error) -> Void in
            
            if( error != nil ) {
                print("Error: \(String(describing: error?.localizedDescription))")
                return
            }
            
            var heightString = ""
            self.height = userHeight as? HKQuantitySample
            
            // The height is formatted to the user's locale.
            if let meters = self.height?.quantity.doubleValue(for: HKUnit.meter()) {
                let formatHeight = LengthFormatter()
                formatHeight.isForPersonHeightUse = true
                heightString = formatHeight.string(fromMeters: meters)
            }
            
            
            // Set the label to reflect the user's height.
            print(heightString)
            self.lblData.text = "Height: " + heightString
            
        })
        
    }

    func setWeight(){
        // Create the HKSample for Weight.(HKQuantityTypeIdentifier called bodyMass)
        let weightSample = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)
        
        // Call HealthKitManager's getSample() method to get the user's weight.
       self.healthManager.getWeight(sampleType: weightSample!, completion: { (userWeight, error) -> Void in
        
            if( error != nil ) {
                print("Error: \(String(describing: error?.localizedDescription))")
                return
            }
            
            var weightString = ""
            
            self.weight = userWeight as? HKQuantitySample
            
            // The weight is formatted to the user's locale.
        if let grams = self.weight?.quantity.doubleValue(for: HKUnit.gram()) {
                let formatWeight = MassFormatter()
                formatWeight.isForPersonMassUse = true
                
            weightString = formatWeight.string(fromKilograms: grams/1000)//convert back to kg
            }
            
            // Set the label to reflect the user's height.
            print(weightString)
        self.lblWeightData.text = "Weight: " + weightString
            
        })
    }
    
   
    let health = HKHealthStore()
    let heartRateUnit : HKUnit = HKUnit(from : "count/min")//they are scalar unit, connot convert
    var heartRateType : HKQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
    var heartRateQuery: HKQuery?
    
    func setHeartRate(){
        
        //predicate
        let calendar = NSCalendar.current as NSCalendar
        let now = NSDate() as Date
        let components = calendar.components([.day, .month, .year ], from : now )
//      components.timeZone = NSTimeZone(abbreviation: "EDT")! as TimeZone
        
        guard let startDate = calendar.date(from: components) else {return}
        let endDate = calendar.date(byAdding: .day, value: 2, to: startDate, options: [])
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: [])

        //descriptor
        let sortDescriptors = [NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)]
        
        //query
        heartRateQuery = HKSampleQuery( sampleType: heartRateType,
                                        predicate: predicate,
                                        limit: 5,
                                        sortDescriptors: sortDescriptors){
                                            (query: HKSampleQuery, results: [HKSample]?, error: Error?) -> Void in
                                            
                                            guard error == nil else { print("error"); return }
                                            
                                            //print out the data in string
                                            self.printHeartRateInfo(results: results as! [HKQuantitySample])
                                            
                                            //convert to JSON object
                                            self.modifyToDictionary(results: results as! [HKQuantitySample])
                                            
        }
        health.execute(heartRateQuery!)
    }
    private func modifyToDictionary(results: [HKQuantitySample]!){
    let number = results.count
        print (number)
        
        //Both are working fine
//        var resultDictionary : [String : Any] = [:]
//        for index in 0..<number{
//            guard let currData:HKQuantitySample = results![index] as? HKQuantitySample else { return }
//
//            let item  : [String : Any] = [ "item" : "\(index)",
//                "Heart Rate" : "\(currData.quantity.doubleValue(for: heartRateUnit))",
//                "quantityType" : "\(currData.quantityType)",
//                "Start Date" : "\(currData.startDate)",
//                "End Date" : "\(currData.endDate)",
//                "Metadata" : "\(String(describing: currData.metadata))",
//                "UUID" : "\(currData.uuid)",
//                "Source" : "\(currData.sourceRevision)",
//                "Device" : "\(String(describing: currData.device))"
//            ]
//
//        // add the value to each item
//        resultDictionary["items\(index)"] = item
//        }
        
        //convert to json object by swiftyJSON
//        let json = JSON(resultDictionary)
//        print(json)
        
        //use this way to create a array, don't use let items:[[String:Any]] =[[:]] , it will make an empty elements first
        var items = [[String : Any]] ()
        for index in 0..<number{
            
            let currData:HKQuantitySample = results![index]
            
            var eachitem  = [String : Any] ()
            
            eachitem["item no."] = index
            eachitem["Heart Rate"] = currData.quantity.doubleValue(for: heartRateUnit)
            //eachitem["Quantity Type"] = (String(describing: currData.quantityType))
            eachitem["Start Date"] = (String(describing: currData.startDate))
            eachitem["End Date"] = (String(describing: currData.endDate))
            //eachitem["Metadata"] = (String(describing: currData.metadata))
            //eachitem["UUID"] = (String(describing: currData.uuid))
            //eachitem["Source"] = (String(describing: currData.sourceRevision))
            //eachitem["Device"] = (String(describing: currData.device))
            
            
            // Array append the dictionary item
            items.append(eachitem)
            
        }
        //convert stringArray to JSON by JSONSerialization
        guard let myJSONData = try? JSONSerialization.data(withJSONObject: items, options: []) else {
                return
        }
        //here stringify the  JSON data, can we call already? No, just for printand read
        let formattedJSON = String(data: myJSONData, encoding: String.Encoding.utf8)
        print (formattedJSON ??  [[String : Any]]())
        print("---------------------------------")
        
       
        //convert by swiftyJSON
        let json = JSON(items)
        print(json)

        //get the specified item example
        let selectedObject = json[1]["Heart Rate"]
        print(selectedObject )
    
    }
    
    private func printHeartRateInfo(results: [HKQuantitySample]!){
        let number = results.count
        print("number in print" + "\(number)")
        var stringPulse = ""
        for index in 0..<number{
            
            let currData:HKQuantitySample = results![index]
            
            stringPulse += "[\(index)]" + "\n"
            stringPulse += "Heart Rate: \(currData.quantity.doubleValue(for: heartRateUnit))" + "\n"
            stringPulse += "quantityType: \(currData.quantityType)" + "\n"
            stringPulse += "Start Date: \(currData.startDate)" + "\n"
            stringPulse += "End Date: \(currData.endDate)" + "\n"
            stringPulse += "Metadata: \(String(describing: currData.metadata))" + "\n"
            stringPulse += "UUID: \(currData.uuid)" + "\n"
            stringPulse += "Source: \(currData.sourceRevision)" + "\n"
            stringPulse += "Device: \(String(describing: currData.device))" + "\n"
            stringPulse += "---------------------------------\n"
        }
        
        //since the output too long, i sent it to console
        print(stringPulse)
        self.lblHeartRateData.text = "print out in console, check console please"
        
    }
    private func getStepCount(){
        let stepCountUnit : HKUnit = HKUnit(from : "count/min")//they are scalar unit, cannot convert, built-in
        var stepCountType : HKQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)!
        var stepCountQuery: HKQuery?
        //predicate
        let calendar = NSCalendar.current
        let now = NSDate() as Date
        let endComponent = calendar.dateComponents([.day, .month, .year ], from : now )
        
        let startDate = calendar.startOfDay(for: now)
        
        guard let endDate = calendar.date(from: endComponent)else {return}
        
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: [])
        
        //descriptor
        let sortDescriptors = [NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)]
        
        //query
        stepCountQuery = HKSampleQuery( sampleType: stepCountType,
                                        predicate: predicate,
                                        limit: 5,
                                        sortDescriptors: sortDescriptors){
                                            (query: HKSampleQuery, results: [HKSample]?, error: Error?) -> Void in
                                            
                                            guard error == nil else { print("error"); return }
                                            
                                            //print out the data in string
                                            
                                            
        }
        health.execute(stepCountQuery!)
    }
    

    @IBAction func Auth(_ sender: Any) {
        // We cannot access the user's HealthKit data without specific permission.
        getHealthKitPermission()
        
    }
    @IBAction func getData(_ sender: Any) {
        setHeight()
        setWeight()
    }
    @IBAction func getHeartRateData (_ sender: Any){
        setHeartRate()
    }
    @IBAction func getStepCountData (_ sender: Any){
        getStepCount()
    }
    func getHealthKitPermission() {
        
        // Seek authorization in HealthKitManager.swift.
        healthManager.authorizeHealthKit { (authorized,  error) -> Void in
            if authorized {
                
                // Get and set the user's height.
                print("authorized")
                //self.setHeight()
            } else {
                if error != nil {
                    print(error ?? "no authorization")
                }
                print("Permission denied.")
            }
    }
}
    

}

